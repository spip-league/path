<?php

namespace SpipLeague\Component\Path;

use Generator;
use SpipLeague\Component\Hasher\Hash32;
use SpipLeague\Component\Hasher\HashInterface;
use Symfony\Component\Filesystem\Path as HelperPath;

use function rtrim;
use function str_starts_with;

/**
 * Aggregate a list of paths where application can search and use files
 */
class Aggregator implements AggregatorInterface
{
    /**
     * @var string[]
     */
    private array $directories = [];

    /**
     * @var array<string,Path>
     */
    private array $paths = [];

    private bool $uptodate = false;

    private string $hash;

    private readonly string $root_directory;

    /**
     * Create directory list
     *
     * @param string[] $directories List of directories in order (relatives to root_directory if defined)
     */
    public function __construct(
        array $directories,
        ?string $root_directory = null,
        private readonly HashInterface $hasher = new Hash32(),
    ) {
        $this->root_directory = rtrim($root_directory ?? (getcwd() ?: '.'), '/') . '/';
        $this->directories = array_unique($this->normalize($directories));
    }

    /**
     * Returns Path instances
     *
     * @return Generator<string,Path>
     */
    public function getIterator(): Generator
    {
        foreach ($this->getDirectories() as $directory) {
            $this->paths[$directory] ??= new Path($this->makeAbsolute($directory));
            yield $directory => $this->paths[$directory];
        }
    }

    public function count(): int
    {
        return count($this->getDirectories());
    }

    /**
     * @return string[]
     */
    public function getDirectories(): array
    {
        $this->update();
        return $this->directories;
    }

    public function getHash(): string
    {
        $this->update();
        return $this->hash;
    }

    private function update(): void
    {
        if ($this->uptodate === false) {
            $this->uptodate = true;
            $this->directories = $this->flatten();
            $this->hash = $this->hasher->hash($this->directories);
        }
    }

    /**
     * @return string[]
     */
    protected function flatten(): array
    {
        return $this->directories;
    }

    /**
     * Normalize the directory list
     *
     * @param string[] $directories
     * @return string[]
     */
    protected function normalize(array $directories): array
    {
        $this->uptodate = false;
        foreach ($directories as $key => $directory) {
            $directories[$key] = $this->canonicalize($directory);
        }

        return $directories;
    }

    private function makeAbsolute(string $directory): string
    {
        if (str_starts_with($directory, '/')) {
            return $directory;
        }

        return $this->canonicalize($this->root_directory . $directory);
    }

    private function canonicalize(string $path): string
    {
        return str_replace('/', \DIRECTORY_SEPARATOR, HelperPath::canonicalize($path));
    }
}
