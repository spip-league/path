<?php

namespace SpipLeague\Component\Path;

use Countable;
use Generator;
use IteratorAggregate;

/**
 * @extends IteratorAggregate<string, Path>
 */
interface AggregatorInterface extends IteratorAggregate, Countable
{
    /**
     * Returns Path instances
     *
     * @return Generator<string,Path>
     */
    public function getIterator(): Generator;

    /**
     * Retuns Count of directories
     */
    public function count(): int;

    /**
     * @return string[] Directory list
     */
    public function getDirectories(): array;

    /**
     * @return string Unique hash for this directory list
     */
    public function getHash(): string;
}
