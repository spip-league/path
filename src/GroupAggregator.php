<?php

namespace SpipLeague\Component\Path;

use UnitEnum;
use SpipLeague\Component\Hasher\Hash32;
use SpipLeague\Component\Hasher\HashInterface;

use function array_unique;

/**
 * Aggregate a list of paths where application can search and use files
 *
 * Immutable: changes directories create clones.
 *
 * Paths are returns in the order of `$groups` (UnitEnum cases),
 * then in order of it’s respective directories.
 */
class GroupAggregator extends Aggregator
{
    /**
     * @var array<string,string[]>
     */
    private array $directories_by_group = [];

    /**
     * Create directory list
     *
     * @param array<int,UnitEnum> $groups List of allowed Enums in order
     */
    public function __construct(
        private readonly array $groups,
        ?string $root_directory = null,
        HashInterface $hasher = new Hash32(),
    ) {
        parent::__construct([], $root_directory, $hasher);
    }

    /**
     * Replace directories in this group
     *
     * @param string[] $directories
     */
    public function with(UnitEnum $group, array $directories): self
    {
        return (clone $this)->setGroup($group, $directories);
    }

    /**
     * Append directories in this group
     *
     * @param string[] $directories
     */
    public function append(UnitEnum $group, array $directories): self
    {
        return (clone $this)->setGroup($group, [...$this->getGroup($group), ...$directories]);
    }

    /**
     * Prepend directories in this group
     *
     * @param string[] $directories
     */
    public function prepend(UnitEnum $group, array $directories): self
    {
        return (clone $this)->setGroup($group, [...$directories, ...$this->getGroup($group)]);
    }

    /**
     * @return string[]
     */
    private function getGroup(UnitEnum $group): array
    {
        return $this->directories_by_group[$group->name] ?? [];
    }

    /**
     * @param string[] $directories
     */
    private function setGroup(UnitEnum $group, array $directories): self
    {
        $this->directories_by_group[$group->name] = array_unique($this->normalize($directories));
        return $this;
    }

    /**
     * @return string[]
     */
    protected function flatten(): array
    {
        $directories = [];
        foreach ($this->groups as $group) {
            $directories = [...$directories, ...$this->directories_by_group[$group->name] ?? []];
        }
        return array_unique($directories);
    }
}
