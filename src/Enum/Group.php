<?php

namespace SpipLeague\Component\Path\Enum;

/**
 * Cases are ordored by priority
 */
enum Group
{
    case ExtraTemplates;
    case Templates;
    case Plugins;
    case App;
}
