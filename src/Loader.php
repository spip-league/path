<?php

namespace SpipLeague\Component\Path;

use Psr\Container\ContainerInterface;
use Psr\SimpleCache\CacheInterface;
use SpipLeague\Component\Hasher\Hash32;
use SpipLeague\Component\Hasher\HashInterface;
use SpipLeague\Component\Path\Exception\RuntimeException;

use function array_key_exists;
use function sprintf;

final class Loader implements ContainerInterface
{
    private readonly string $hash;

    private readonly string $key;

    /**
     * @var array<string,string|false>
     */
    private array $files = [];

    /**
     * @var array<string,bool>
     */
    private array $included = [];

    public function __construct(
        private readonly AggregatorInterface $paths,
        private readonly CacheInterface $cache,
        private readonly HashInterface $hasher = new Hash32(),
    ) {
        $this->key = $paths->getHash();
        /** @var null|array{hash: string, count: int, paths: array<string>, files: array<string,string>} */
        $data = $this->cache->get($this->key);
        $this->hash = $data['hash'] ?? '';
        $this->files = $data['files'] ?? [];
    }

    /**
     * Write cache if content changes when destruct
     */
    public function __destruct()
    {
        $hash = $this->hasher->hash($this->files);
        if ($hash !== $this->hash) {
            $this->cache->set($this->key, [
                'hash' => $hash,
                'count' => $this->paths->count(),
                'paths' => $this->paths->getDirectories(),
                'files' => $this->files,
            ]);
        }
    }

    public function has(string $file): bool
    {
        return array_key_exists($file, $this->files) ?: $this->search($file);
    }

    public function get(string $file): ?string
    {
        return $this->has($file) ? ($this->files[$file] ?: null) : null;
    }

    public function include(string $file): ?string
    {
        $path = $this->get($file);
        if ($path === null) {
            return null;
        }
        if (!array_key_exists($file, $this->included)) {
            include_once $path;
            $this->included[$file] = true;
        }
        return $path;
    }

    public function require(string $file): mixed
    {
        $path = $this->include($file);
        if ($path === null) {
            throw new RuntimeException(sprintf('Can’t require "%s" file : not found', $file));
        }
        return $path;
    }

    public function getCache(): CacheInterface
    {
        return $this->cache;
    }

    private function search(string $file): bool
    {
        foreach ($this->paths as $path) {
            if ($path->has($file)) {
                $this->set($file, $path->get($file));
                return true;
            }
        }
        $this->set($file, false);
        return false;
    }

    private function set(string $file, string|false|null $path): void
    {
        $this->files[$file] = $path ?? false;
    }
}
