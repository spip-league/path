<?php

namespace SpipLeague\Component\Path;

use Psr\Container\ContainerInterface;

use function array_key_exists;
use function dirname;
use function file_exists;
use function is_dir;
use function rtrim;

/**
 * Manage a search of file in a directory.
 *
 * @internal
 */
final class Path implements ContainerInterface
{
    /**
     * @var array<string,bool>
     */
    private array $files = [];

    /**
     * @var array<string,bool>
     */
    private array $dirs = [];

    public readonly string $directory;

    public function __construct(string $directory)
    {
        $this->directory = rtrim($directory, '/');
    }

    public function has(string $file): bool
    {
        if (array_key_exists($file, $this->files)) {
            return $this->files[$file];
        }
        return $this->files[$file] = $this->search($file);
    }

    public function get(string $file): ?string
    {
        return $this->has($file) ? $this->directory . \DIRECTORY_SEPARATOR . $file : null;
    }

    private function getDirname(string $file): string
    {
        $dirname = dirname($file);
        if ($dirname === '.') {
            $dirname = '';
        }
        return $dirname;
    }

    private function search(string $file): bool
    {
        $dirname = $this->getDirname($file);

        $this->dirs[$dirname] ??= is_dir($this->directory . ($dirname ? \DIRECTORY_SEPARATOR . $dirname : ''));
        if (
            $this->dirs[$dirname] === true
            && @file_exists($this->directory . \DIRECTORY_SEPARATOR . $file)
        ) {
            return true;
        }

        return false;
    }
}
