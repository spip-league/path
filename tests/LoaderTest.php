<?php

namespace SpipLeague\Test\Component\Path;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Depends;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use SpipLeague\Component\Cache\Adapter\FlatFilesystem;
use SpipLeague\Component\Path\Aggregator;
use SpipLeague\Component\Path\Loader;
use SpipLeague\Component\Path\Path;

#[CoversClass(Loader::class)]
#[CoversClass(Aggregator::class)]
#[CoversClass(Path::class)]
class LoaderTest extends TestCase
{
    protected function getLoader(): Loader
    {
        return new Loader(new Aggregator(['../src', '../tests'], __DIR__), new FlatFilesystem('test-loader'));
    }

    public function testLoader(): void
    {
        $loader = $this->getLoader();
        $loader
            ->getCache()
            ->clear();

        $loader = $this->getLoader();
        $files = Reflection::getProperty($loader, 'files');
        $this->assertIsArray($files);
        $this->assertCount(0, $files);

        $this->assertFalse($loader->has('nothing.php'));
        $this->assertTrue($loader->has('Loader.php'));

        $files = Reflection::getProperty($loader, 'files');
        $this->assertIsArray($files);
        $this->assertCount(2, $files);

        $this->assertNull($loader->get('nothing.php'));
        $this->assertSame(dirname(__DIR__) . '/src/Loader.php', $loader->get('Loader.php'));
    }

    #[Depends('testLoader')]
    public function testLoaderCache(): void
    {
        $loader = $this->getLoader();
        $files = Reflection::getProperty($loader, 'files');
        $this->assertIsArray($files);
        $this->assertCount(2, $files);
    }

    #[Depends('testLoaderCache')]
    public function testLIncludes(): void
    {
        $loader = $this->getLoader();
        $this->assertNotNull($loader->include('data/include.php'));
        $this->assertTrue(function_exists('SpipLeague\Test\Component\Path\data_include_test'));

        $this->assertNotNull($loader->require('data/require.php'));
        $this->assertTrue(function_exists('SpipLeague\Test\Component\Path\data_require_test'));

        $this->assertNull($loader->include('data/not.exists.php'));
        $this->expectException(RuntimeException::class);
        $loader->require('data/not.exists.php');
    }

    public static function tearDownAfterClass(): void
    {
        (new FlatFilesystem('test-loader'))->clear();
    }
}
