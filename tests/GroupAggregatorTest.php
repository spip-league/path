<?php

namespace SpipLeague\Test\Component\Path;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use SpipLeague\Component\Path\Aggregator;
use SpipLeague\Component\Path\Enum\Group;
use SpipLeague\Component\Path\GroupAggregator;
use SpipLeague\Component\Path\Path;

#[CoversClass(GroupAggregator::class)]
#[CoversClass(Aggregator::class)]
#[CoversClass(Path::class)]
class GroupAggregatorTest extends TestCase
{
    public function testGeneral(): void
    {

        $base = ['../src', '../tests'];
        $extra = ['../vendor', '../nothing'];
        $templates = ['squelettes-lab'];
        $other_templates = ['squelettes'];

        $groups = Group::cases();
        $paths = new GroupAggregator($groups);

        $this->assertSame(0, $paths->count());
        $this->assertSame([], $paths->getDirectories());

        $paths = $paths->with(Group::App, ['src', 'tests/', '.', '', '../', '/absolute']);
        $this->assertSame(['src', 'tests', '', '..', '/absolute'], $paths->getDirectories());
        $this->assertSame(5, $paths->count());

        $paths = new GroupAggregator($groups);
        $new_paths = $paths->with(Group::App, $base);
        $this->assertFalse($paths === $new_paths);
        $paths = $new_paths;

        $hash = $paths->getHash();
        $this->assertSame(2, $paths->count());
        $this->assertSame($base, $paths->getDirectories());

        $paths = $paths->with(Group::Plugins, $extra);
        $this->assertSame(4, $paths->count());
        $this->assertSame([...$extra, ...$base], $paths->getDirectories());
        $this->assertNotSame($hash, $paths->getHash());

        $paths = $paths->with(Group::Templates, $templates);
        $this->assertSame(5, $paths->count());
        $this->assertSame([...$templates, ...$extra, ...$base], $paths->getDirectories());

        // replace, no duplicate
        $paths = $paths->with(Group::Templates, $other_templates);
        $this->assertSame(5, $paths->count());
        $this->assertSame([...$other_templates, ...$extra, ...$base], $paths->getDirectories());
    }

    public function testAppendPrepend(): void
    {
        $groups = Group::cases();
        $paths = new GroupAggregator($groups);
        $this->assertSame(0, $paths->count());

        $paths = $paths->append(Group::App, ['A']);
        $this->assertSame(1, $paths->count());
        $this->assertSame(['A'], $paths->getDirectories());

        $paths = $paths->append(Group::App, ['B']);
        $this->assertSame(2, $paths->count());
        $this->assertSame(['A', 'B'], $paths->getDirectories());

        $paths = new GroupAggregator($groups);
        $paths = $paths->prepend(Group::App, ['A']);
        $this->assertSame(1, $paths->count());
        $this->assertSame(['A'], $paths->getDirectories());

        $paths = $paths->prepend(Group::App, ['B']);
        $this->assertSame(2, $paths->count());
        $this->assertSame(['B', 'A'], $paths->getDirectories());

        $paths = $paths->append(Group::App, ['C']);
        $this->assertSame(3, $paths->count());
        $this->assertSame(['B', 'A', 'C'], $paths->getDirectories());

        // no duplicates
        $paths = $paths->prepend(Group::App, ['C']);
        $this->assertSame(3, $paths->count());
        $this->assertSame(['C', 'B', 'A'], $paths->getDirectories());

        $paths = $paths->append(Group::App, ['C']);
        $this->assertSame(3, $paths->count());
        $this->assertSame(['C', 'B', 'A'], $paths->getDirectories());
    }

    public function testNoDuplicate(): void
    {
        $groups = Group::cases();
        $paths = new GroupAggregator($groups);
        $dirs = ['A', 'B', 'C'];
        $paths = $paths->with(Group::App, $dirs);
        $paths = $paths->with(Group::Plugins, $dirs);
        $paths = $paths->with(Group::Templates, $dirs);
        $paths->count();
        $this->assertSame(3, $paths->count());
        $this->assertSame(['A', 'B', 'C'], $paths->getDirectories());
    }

    public function testAggregatorIterator(): void
    {
        $groups = Group::cases();
        $paths = new GroupAggregator($groups);
        $paths = $paths->with(Group::App, ['../src']);
        $paths = $paths->with(Group::Plugins, ['../tests']);
        foreach ($paths as $path) {
            $this->assertSame(Path::class, $path::class);
        }
        $list = Reflection::getParentProperty($paths, 'paths');
        $this->assertIsArray($list);
        $this->assertCount(2, $list);
    }
}
