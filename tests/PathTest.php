<?php

namespace SpipLeague\Test\Component\Path;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use SpipLeague\Component\Path\Path;

#[CoversClass(Path::class)]
class PathTest extends TestCase
{
    public function testPath(): void
    {
        $path = new Path(__DIR__);
        $this->assertFalse($path->has('nothing.php'));
        $this->assertTrue($path->has(basename(__FILE__)));
        $this->assertNull($path->get('nothing.php'));
        $this->assertSame(__FILE__, $path->get(basename(__FILE__)));

        $path = new Path(dirname(__DIR__));
        $this->assertFalse($path->has('tests/nothing.php'));
        $this->assertTrue($path->has('tests/' . basename(__FILE__)));
        $this->assertNull($path->get('tests/nothing.php'));
        $this->assertSame(__FILE__, $path->get('tests/' . basename(__FILE__)));
    }
}
