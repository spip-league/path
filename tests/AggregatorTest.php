<?php

namespace SpipLeague\Test\Component\Path;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\TestCase;
use SpipLeague\Component\Path\Aggregator;
use SpipLeague\Component\Path\Path;

#[CoversClass(Aggregator::class)]
#[CoversClass(Path::class)]
class AggregatorTest extends TestCase
{
    public function testAggregator(): void
    {
        $base = ['../src/', '../tests', '', './'];
        $paths = new Aggregator($base);
        $this->assertSame(['../src', '../tests', ''], $paths->getDirectories());
        $this->assertSame(3, $paths->count());
    }

    public function testAggregatorHash(): void
    {
        $base = ['../src', '../tests'];
        $paths = new Aggregator($base);
        $this->assertIsString($a = $paths->getHash());

        $paths = new Aggregator($base);
        $this->assertEquals($a, $paths->getHash());

        $paths = new Aggregator(['../src']);
        $this->assertNotEquals($a, $paths->getHash());
    }

    public function testAggregatorIterator(): void
    {
        $paths = new Aggregator(['../src', '../tests']);
        foreach ($paths as $path) {
            $this->assertSame(Path::class, $path::class);
        }
        $list = Reflection::getProperty($paths, 'paths');
        $this->assertIsArray($list);
        $this->assertCount(2, $list);
    }

    public function testNormalize(): void
    {
        $paths = new Aggregator(['src']);
        $dirs = Reflection::callMethod($paths, 'normalize', [['', '/truc', '.', '..', './', '../']]);
        $this->assertEquals(['', '/truc', '', '..', '', '..'], $dirs);
    }

    public function testMakeAbsolute(): void
    {
        $paths = new Aggregator(['src/'], root_directory: __DIR__);

        $dir = Reflection::callMethod($paths, 'makeAbsolute', ['']);
        $this->assertEquals(__DIR__, $dir);

        $dir = Reflection::callMethod($paths, 'makeAbsolute', ['/truc']);
        $this->assertEquals('/truc', $dir);
    }
}
