<?php

namespace SpipLeague\Test\Component\Path;

use ReflectionClass;

class Reflection
{
    public static function getProperty(object $object, string $property): mixed
    {
        $class = new ReflectionClass($object);
        $property = $class->getProperty($property);
        $property->setAccessible(true);
        return $property->getValue($object);
    }

    public static function getParentProperty(object $object, string $property): mixed
    {
        $class = new ReflectionClass($object);
        $parent = $class->getParentClass();
        if ($parent === false) {
            throw new \RuntimeException(sprintf('Can’t get parent class of %s', $object::class));
        }
        $property = $parent->getProperty($property);
        $property->setAccessible(true);
        return $property->getValue($object);
    }

    /**
     * @param array<string|int,mixed> $args
     */
    public static function callMethod(object $object, string $method, array $args = []): mixed
    {
        $class = new ReflectionClass($object);
        $method = $class->getMethod($method);
        $method->setAccessible(true);
        return $method->invokeArgs($object, $args);
    }
}
