# Path component

This component find files in a list of directories.

## Note

It’s somehow like a tiny part of symfony/finder with a simple search in multiple directories.
It’s faster than symfony/finder for this special task.

## Installation

```bash
composer require spip-league/path
```

## Usage

### Basic usage

There are 3 classes to manage that (`Path` class is internal) :

- the `Aggregator` operates an iterator of `Path` (directory information)
- the `GroupAggregator` extends `Aggregator` to use multiple groups of directories
- the `Loader` search & cache files found, by traversing the `Aggregator`

```php
use SpipLeague\Component\Cache\Adapter\FlatFilesystem;
use SpipLeague\Component\Path\Aggregator;
use SpipLeague\Component\Path\Loader;

$loader = new Loader(
    new Aggregator(['src/', 'tests/'], __DIR__),
    new FlatFilesystem('paths')
);
$loader->has('Aggregator.php'); // bool
$loader->get('Exception/RuntimeException.php'); // string filepath | null

// do include_once, if file found, returns filepath or null
$loader->include('inc/utils.php'); 
// like include but throws a RuntimeException if not found
$loader->require('inc/utils.php'); 
```

### Aggregator usage

The `Aggregator` implements an `AggregatorInterface` and takes directly
in constructor a finite list of directories

- `array $directories`:  list of (string) directories in first argument
- `string $root_directories`: (optionnal) location of the root directory
- [Other optionnal interfaces](src/Aggregator.php#L50)

```php
use SpipLeague\Component\Path\Aggregator;
use SpipLeague\Component\Path\Path;

$aggregator = new Aggregator(['src/', 'tests/'], __DIR__);
$aggregator->count(); // 2
$aggregator->getDirectories(); // ['src', 'tests']
$aggregator->getHash(); // (string) hash of the directory list

/** @var Path $path */
foreach ($aggregator as $path) {
    $path->hash('file.php'); // bool
    $path->get('file.php'); // ?string full path file
    $path->directory; // string base path directory
}
```

### GroupAggregator usage

The `GroupAggregator` extends `Aggregator` and manage group of directories.

- `array $groups`:  list of (UnitEnum) ordered group types in first argument
- `string $root_directories`: (optionnal) location of the root directory
- [Other optionnal interfaces](src/GroupAggregator.php#L42)

```php
use SpipLeague\Component\Cache\Adapter\FlatFilesystem;
use SpipLeague\Component\Path\GroupAggregator;
use SpipLeague\Component\Path\Loader;

enum MyGroup {
    case App;
    case Plugins;
    case Templates;
}

$aggregator = new GroupAggregator(MyGroup::cases(), __DIR__);
$aggregator = $aggregator->with(MyGroup::App, ['src/', 'tests/']);
$aggregator = $aggregator->with(MyGroup::Templates, ['squelettes/']);

// append() add directories in end of the group, even if empty
$aggregator = $aggregator->append(MyGroup::Plugins, ['A']); // ['A']
// prepend() add directories in front of the group, even if empty
$aggregator = $aggregator->prepend(MyGroup::Plugins, ['B']); // ['B', 'A']
// with() replaces all the group
$aggregator = $aggregator->with(MyGroup::Plugins, ['C']); // ['C']
$aggregator = $aggregator->prepend(MyGroup::Plugins, ['D', 'E']); // ['D', 'E', 'C']
$aggregator = $aggregator->append(MyGroup::Plugins, ['F']); // ['D', 'E', 'C', 'F']

$aggregator->count(); // 7
$aggregator->getDirectories(); // ['squelettes', 'D', 'E', 'C', 'F', 'src', 'tests']
$aggregator->getHash(); // (string) hash of the directory list

/** @var Path $path */
foreach ($aggregator as $path) {
    $path->hash('file.php'); // bool
    $path->get('file.php'); // ?string path
    $path->directory; // root directory path
}
```

### Loader

The `Loader` takes some interfaces (Aggregator, Simple Cache),
and for each search of file done, it returns (and cache the path) of the first
file found in the Aggregator list.

If directory list changes in the execution, you should cache
the loader by hash of the Aggregator, like :

```php
function loader(AggregatorInterface $aggregator): Loader {
    static $loaders = [];
    return $loaders[$aggregator->getHash()] ??= new Loader(
        $aggregator,
        new FlatFilesystem('paths')
    );
}
```

## Tests

See Composer scripts in [composer.json](composer.json#L46)

```bash
composer test
composer test-coverage
```
