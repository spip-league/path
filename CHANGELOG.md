# Changelog

## Unreleased

## 2.1.0 - 2024-12-21

- require `spip-league/filesystem=^3.0`

## 2.0.0 - 2024-07-18

### Changed

- namespace PHP de `Spip` à `SpipLeague`
- PHP 8.2 mini
